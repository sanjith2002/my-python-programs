#to encrypt text using xor encryption
print('1. to encrypt your plain text using XOR encryption\n2. to decrypt your cipher text using XOR decryption')
m=int(input('enter your input[1/2]:'))
if m == 1:
    a=input('enter your plain text:')
    b=input('enter your key:')
    c="".join(chr(ord(i) ^ ord(j)) for i,j in zip(a,b))
    print('your encrypted text is:',c)
elif m == 2:
    a=input('enter your cipher text:')
    b=input('enter your key:')
    c="".join(chr(ord(i) ^ ord(j)) for i,j in zip(a,b))
    print('your decrypted text is:',c)